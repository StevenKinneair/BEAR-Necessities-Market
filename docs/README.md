﻿<style>
  img[alt=logo] {
    display: block;
    max-width: 18%;
    height: auto;
    margin: auto;
    float: none!important;
  }
  img[alt=logo-title-text] {
    display: block;
    max-width: 65%;
    height: auto;
    margin: auto;
    float: none!important;
  }
</style>
![logo](./logo.png)
![logo-title-text](./logo-title-text.png)

<!--
[![GitLab license](https://img.shields.io/badge/license-GPL%20V3-blue.svg)](./LICENSE.md)
[![Build Status](https://travis-ci.com/LibreFoodPantry/BEAR-Necessities-Market.svg?branch=master)](https://travis-ci.com/LibreFoodPantry/BEAR-Necessities-Market)
[![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/)
[![npm version](https://img.shields.io/npm/v/react.svg?style=flat)](https://www.npmjs.com/package/react)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](https://reactjs.org/docs/how-to-contribute.html#your-first-pull-request)
[![Known Vulnerabilities](https://snyk.io/test/github/dwyl/hapi-auth-jwt2/badge.svg?targetFile=package.json)](https://snyk.io/test/github/dwyl/hapi-auth-jwt2?targetFile=package.json)
[![Dependencies](https://img.shields.io/badge/dependencies-up%20to%20date-brightgreen.svg)](https://img.shields.io/badge/dependencies-up%20to%20date-brightgreen.svg)
-->

An online ordering system for food pantries.

This project is a LibreFoodPantry project. As such, we share its policies, practices, and infrastructure. Please visit LibreFoodPantry.org for more information.

- Source code licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt)
- Content licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
- [Code of Conduct](https://librefoodpantry.org/#/overview/code-of-conduct/)
- [Discord server](https://discord.gg/PRth8YK)
- [Source code and issue tracker](https://gitlab.com/LibreFoodPantry/BEAR-Necessities-Market)
- [What's New](./CHANGELOG.md)

---
["pantry"](https://thenounproject.com/search/?q=food%20pantry&i=489212#) icon by David Carrero from the [the Noun Project](https://thenounproject.com/)

Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
